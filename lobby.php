<?php
	require_once("action/LobbyAction.php");

	$action = new LobbyAction();
	$action->execute();

	$result = $action->result;

	

?>
	<!DOCTYPE html>
	<html lang="fr">
    <head>
        <link href="css/global.css" rel="stylesheet" />
		<title>Sirius</title>
		<meta charset="utf-8" />
		<script src="js/jquery.min.js"></script>
		<script src="js/TiledImage.js"></script>
		<script src="js/javascriptLobby.js"></script>
		<script src="sprites/Missile.js"></script>
		
		<script>
		$(function () {
			// On attend deux secondes puis on fait une demande de renseignement sur le jeu et le joueur
			setTimeout(infos, 2000);
			setTimeout(list, 2000);
			
		})
		function infos(){
				$.ajax({
					url: "ajax-user-info.php",
					type: "POST",
					data: {}
				})
				.done(function (data) {

					let info = JSON.parse(data);
					if(typeof info !== "object"){
                        if(info == "USER_NOT_FOUND"){
							window.location.assign("index.php");
						}
					}
					let charFrame = document.getElementById("lobby-frame");
					// On efface le contenu du frame de gauche contenant les infos
					// afin d'afficher les dernieres infos recues
					while (charFrame.firstChild) {
						charFrame.removeChild(charFrame.firstChild);
						}
					
					// On récupere le template du character (joueur)
					let template = document.getElementById("char-template");
					
					// On se créer un noeud et on le met a l'image du template
					let node = document.createElement("div");
					node.innerHTML = template.innerHTML;
					
					// Tous les champs du noeud sont rempli des info récupérer du serveur
					node.querySelector("#name").innerHTML = "Nom : " + info.username;
					node.querySelector("#hp").innerHTML = "HP : " + info.hp;
					node.querySelector("#mp").innerHTML = "MP : " + info.mp;
					node.querySelector("#niveau").innerHTML = "Niveau : " + info.level;
					node.querySelector("#experience").innerHTML = "Experience : " + info.exp + " / " + info.next_level_exp;
					node.querySelector("#victoire").innerHTML = "Victoires : " + info.victories;
					node.querySelector("#defaite").innerHTML = "Defaites : " + info.loss;
					node.querySelector("#feinte").innerHTML = "Feinte : " + info.dodge_chance + " %";
					node.querySelector("#armure").innerHTML = "Armure : " + info.dmg_red;
					charFrame.appendChild(node);
					
					// On refait une demande d'info dans 2 secondes
					setTimeout(infos, 2000); 
					
				})
				.fail(function (data) {
					alert("Aïe, erreur avec le serveur");
				})
			}
	function list() {
		$.ajax({
			url: "ajax-list-game.php",
			type: "POST",
			data: {}
		})
		.done(function (data) {
			let liste = JSON.parse(data);
			// On récupere la section des parties
			let gameList = document.getElementById("list-games");

			// On supprime tout ce qu'elle contient afin de pouvoir raffraichir avec les dernieres infos
			while (gameList.firstChild) {
   			 gameList.removeChild(gameList.firstChild);
			}
			// On met le titre de l'encadré
			let intro = document.createElement("h2");
			let textintro = document.createTextNode("Liste des parties")
			intro.appendChild(textintro);
			gameList.appendChild(intro);


			let template = document.getElementById("game-template");
			// Pour chacun des élément de la liste récupéré du serveur
			// On fait un noeud qui contiendra toutes les infos d'une partie
			for(let i = 0; i<liste.length;i++){
			let node = document.createElement("div");
			let item = liste[i];
			node.innerHTML = template.innerHTML;
			node.querySelector(".id").innerHTML = item.id;
			node.querySelector(".id_game").value = item.id;
			node.querySelector(".name").innerHTML = item.name;
			node.querySelector(".level").innerHTML = item.level;
			node.querySelector(".nb").innerHTML = item.nb + "/" + item.max_users;
			node.querySelector(".hp").innerHTML = item.hp;
			gameList.appendChild(node);
			}

		// On demande encore les information de parties dans 2 secondes
		setTimeout(list, 2000); 
			
		})
        .fail(function (data) {
            alert("Aïe, erreur avec le serveur");
        })
	
	}	// CI_BAS
		// Le squelette de l'information du joueur puis 
		// le squelette de l'information relatif a une partie
		</script>
		<script type= "char-template" id="char-template">
			
				<h2> Infos du joueur </h2>
				<h3 id="name">Nom : </h3>
				<h3 id="hp">HP : </h3>
				<h3 id="mp">MP : </h3>
				<h3 id="niveau">Niveau : </h3>
				<h3 id="experience">Experience : </h3>
				<h3 id="victoire">Victoires : </h3>
				<h3 id="defaite">Défaites : </h3>
				<h3 id="feinte">Chance de feintes : </h3>
				<h3 id="armure">Armure : </h3>
			
		</script>

		
		<script type="g-template" id="game-template" >
			<div class ="game">
				<form action="lobby.php" method="post">
					<div class="label-game">
						<label for="id">#</label>
					</div>
					<div class="form-input">
						<h3 class="id" name="id"></h3>
					</div>
					<input type="text" name="id_game" class="id_game" style="display:none"/>
					<div class="label-game">
						<label for="name">Nom : </label>
					</div>
					<div class="form-input">
						<h3 class="name" ></h3>
					</div>
					<div class="form-separator"></div>
					<div class="label-game">
						<label for="level">Niveau : </label>
					</div>
					<div class="form-input">
						<h3 class="level"></h3>
					</div>
					<div class="form-separator"></div>
					<div class="label-game">
						<label for="nb">Nombre de joueurs: </label>
					</div >
					<div class="form-input">
						<h3 class="nb"></h3>
					</div>
					<div class="form-separator"></div>
					<div class="label-game">
						<label for="hp">HP : </label>
					</div>
					<div class="form-input">
						<h3 class="hp"></h3>
					</div>
					<div class="label-game">
						<button type="submit">Rejoindre</button>
					</div>
				</form>
			</div>
		</script>
    </head>
    <body>
	<header>
		<h1>Sirius</h1>
		<div class="clear"></div>
	</header>
		
	
	<h2 id="Lobby-Index">Lobby</h2>
	<h3 id="deconnexion"><a href="logout.php">Déconnexion</a></h3>
	<?php // Si il y a un erreur on l'affiche dans la section erreur
				if ($result) {
					?>
					<div class="error-div" style="z-index:3"><strong>Erreur : </strong><?= $result ?></div>
					<?php
				}
			?>
	<div class="lobby-frame" id="lobby-frame">
		
	</div>
	<audio id="audioLobby">
		<source src="audio\lobby.mp3">
	</audio>
	<div id="list-games">
		<h2> Liste des parties </h2>
	</div>

	<canvas id="canvas" width="1920" height="1080">
    </canevas>

<?php
	require_once("partial/footer.php");
