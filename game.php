<?php
	require_once("action/GameAction.php");

	$action = new GameAction();
	$action->execute();

	$infoGame = $action->infoGame;

?>

<!DOCTYPE html>
	<html lang="fr" id="fondjeu">
    <head>
        <link href="css/global.css" rel="stylesheet" />
		<title>Sirius</title>
		<meta charset="utf-8" />
        <script src="js/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="js/TiledImage.js"></script>
        <script src="js/javascriptGame.js"></script>
        <script src="sprites/Joueur.js"></script>
        <script src="sprites/Boss.js"></script>
        <script src="sprites/Allie.js"></script>
        

    <script>
        $(function () {
            // On attend 2 secondes avant de faire la demande d'infos de partie
			setTimeout(infosGame, 2000);
        })
        function infosGame(){
				$.ajax({
					url: "ajax-game-info.php",
					type: "POST",
					data: {}
				})
				.done(function (data) {
					
					let info = JSON.parse(data);
                    if(typeof info !== "object"){
                        // Si on obtien un message de victoire on se dirige vers la page de victoire
                        if(info == "GAME_NOT_FOUND_WIN"){
                            window.location.assign("win.php");
                        }// Si on obtient un message de defait, on se dirige vers la page de défaite
                        else if(info == "GAME_NOT_FOUND_LOST"){
                            window.location.assign("lost.php");
                        }
                        else{
                            // Si on a quelques autres messages d'erreurs (clé invalide, joueur déconnecté par le jeu car absent ou encore banni, on se dirige vers la page de login
                            window.location.assign("index.php");
                        }
                    }
                    else{
                        // On vérifie si le boss a attaquer
                        bossAttaque = info.game.attacked;
                        
                        // On créer maintenant les trois boutons d'attaque du bas de l'écran
                        let barreAction = document.getElementById("actions");
                        let template = document.getElementById("info-action-template");
                        // On enleve toutes les boutons d'actions et on le recréer
                        while (barreAction.firstChild) {
                            barreAction.removeChild(barreAction.firstChild);
                            }
                        // Pour chacun des actions possible du joueur, on créer une section + bouton de cette action
                        for(let i = 0; i<info.player.skills.length;i++){
                                noAttaque = i+1;
                                let skill = info.player.skills[i];
                                let node = document.createElement("div");
                                node.className = "Action";
                                node.id = skill.name;
                                node.innerHTML = template.innerHTML;
                                //On inscrit les infos relatives a l'attaque près du bouton
                                node.querySelector(".infoAction").innerHTML = skill.name + " | Level : " + skill.level + " | Cout : " + skill.cost + " | Degat : " + skill.dmg;
                                //si le joueur n'a pas encore attaquer , on place le bouton fonctionnel
                                if(!att){
                                    node.querySelector("button").onclick = function(){
                                    // Lorsqu'on clique sur le bouton, tous les boutons sont désactivés et
                                    // on fait disparaitre légérement toutes les boutons
                                    let tabAction = document.querySelectorAll("button");
                                    for(s=0;s<tabAction.length;s++){
                                        tabAction[s].disabled = true;
                                        tabAction[s].style.opacity = 0.5;
                                        }
                                    // On active l'annonciateur de l'attaque
                                    att = true;
                                    attaque(skill.name,noAttaque);// Appel a la fonction d'Attaque de l'API
                                    }
                                }
                                else{
                                // Sinon on rend le bouton inactif afin d'éviter le spam d'attaque
                                node.querySelector("button").disabled = true;
                                node.querySelector("button").style.opacity = 0.5;
                                }
                            barreAction.appendChild(node);
                        }
                            
                        }
                        // On récupere la section infos des joueurs
                        let barreEtat = document.getElementById("infos");

                        // On efface toutes les infos afin d'afficher les plus récentes infos
                        while (barreEtat.firstChild) {
                            barreEtat.removeChild(barreEtat.firstChild);
                            }

                        
                        template = document.getElementById("info-joueur-template");
                        
                        // Pour chacun des joueurs , nous allons faire une section contenant ses informations de partie
                        node = document.createElement("div");
                        node.className = "infoJoueur";
                        node.innerHTML = template.innerHTML;
                        node.querySelector(".nom").innerHTML = info.player.name;
                        node.querySelector(".portrait").style.backgroundImage = "url('images/portrait-player1.png')";
                        node.querySelector(".level").innerHTML = "Level : " + info.player.level;
                        node.querySelector(".type").innerHTML = "Type : " + info.player.type;
                        node.querySelector(".hp").innerHTML = "HP : "+ info.player.hp + "/"+info.player.max_hp;
                        node.querySelector(".mp").innerHTML = "MP : "+ info.player.mp + "/" + info.player.max_mp;
                        barreEtat.appendChild(node);
                        // Si il y a d'autres joueurs a notre partie, on leur créer une section d'information aussi
                        if(info.other_players.length){
                            nombreJoueur = info.other_players.length + 1; // On stock le nombre de joueur afin l'utiliser dans le Javascript plus tard
                            for(let i = 0; i<info.other_players.length;i++){
                                let ami = info.other_players[i];

                                if(info.other_players[i].attacked != "--"){ // On vérifie si le joueur allié a attaquer
                                    allieAttaque[i] = true;
                                }
                                node = document.createElement("div");
                                node.className = "infoJoueur";
                                node.innerHTML = template.innerHTML;
                                // On associe le portrait du joueur avec le robot correspondant
                                if(i==0)
                                    node.querySelector(".portrait").style.backgroundImage = "url('images/portrait-player2.png')";
                                else if(i==1)
                                    node.querySelector(".portrait").style.backgroundImage = "url('images/portrait-player3.png')";
                                else if(i==2)
                                    node.querySelector(".portrait").style.backgroundImage = "url('images/portrait-player4.png')";
                                node.querySelector(".nom").innerHTML = ami.name;
                                node.querySelector(".level").innerHTML = "Level : " + ami.level;
                                node.querySelector(".type").innerHTML = "Type : " + ami.type;
                                node.querySelector(".hp").innerHTML = "HP : "+ ami.hp + "/"+ami.max_hp;
                                node.querySelector(".mp").innerHTML = "MP : "+ ami.mp + "/" + ami.max_mp;
                                barreEtat.appendChild(node);
                                }
                        }

                        // On récupere le template du boss et on affiche toutes le informations relatives dans sa section
                        template = document.getElementById("info-boss-template");
                        node = document.createElement("div");
                        node.className = "infoBoss";
                        node.innerHTML = template.innerHTML;
                        node.querySelector(".portrait").style.backgroundImage = "url('images/portrait-boss.png')";
                        node.querySelector(".nom").innerHTML = info.game.name;
                        node.querySelector(".level").innerHTML = "Level : " + info.game.level;
                        node.querySelector(".type").innerHTML = "Type : "+ info.game.type;
                        node.querySelector(".hp").innerHTML = "HP : "+ info.game.hp + "/"+info.game.max_hp;
                        barreEtat.appendChild(node);
                        
                        // On fait la demande de nouvelles infos dans 2 secondes
                        setTimeout(infosGame, 2000);
                    
				})
				.fail(function (data) {
					alert("Aïe, erreur avec le serveur");
				})
			}
    </script>
    </head>
    <body >
    
    <audio id="audioGame">
		<source src="audio\combat.mp3">
	</audio>
	<header>

    </header>
    <script type= "info-joueur-template" id="info-joueur-template">
                <div class="portrait" ></div>
                <h3 class="nom">
                </h3>
                <h3 class="level">
                </h3>
                <h3 class="type">
                </h3>
                <h3 class="hp">
                </h3>
                <h3 class="mp">
                </h3>
            
    </script>
    <script type="info-boss-template" id="info-boss-template">
                <div class="portrait" ></div>
                <h3 class="nom">
                </h3>
                <h3 class="level">
                </h3>
                <h3 class="type">
                </h3>
                <h3 class="hp">
                </h3>
                
            
    </script>
    <script type = "info-action-template" id="info-action-template">
            <h3 class="infoAction">
            </h3>
            <button class="actionbutton">Attaque !</button>
            
    </script>
        <div id="barreEtat">
            <h2 class="titreBarre">Barre d'états</h2>
            <div id="infos">
            </div>
        </div>

        <div id="barreAction">
            <h2 class="titreBarre">Barre d'actions</h2>
            <div class="error-div" style="display:none"></div>
            <div id="actions">
            </div>
        </div>
        <canvas id="jeu" width="1024" height="384">
        </canvas>
        <?php
	require_once("partial/footer.php");