class Joueur{

    constructor(path,columnCount,rowCount,refreshDelay,loopColumns,scale,minInterval,maxInterval){

        this.image = new TiledImage(path, columnCount, rowCount, refreshDelay, loopColumns, scale, null);
        this.image.changeRow(0);				
        this.image.changeMinMaxInterval(minInterval, maxInterval);
        this.attacking = false;
        this.nomAttaque = "";
        this.x = 400;
        this.y = 340;
        this.bullet;
        this.bulletX=400; 	
        }

        tick(context){
            // A chaque tick , on vérifie si le joueur est en train d'attaquer
            if(!this.attacking){
                this.image.tick(this.x,this.y,context); 
            }
            else{
                // Si il attaque, il se déplace de facon a ce que son attaque touche le Boss
                // après chacune des attaque, le joueur retourne a son sprite de base (idlestate)
                if(this.nomAttaque === "Normal"){
                    if(this.x <700){
                        this.x += 7;
                    }
                    this.image.tick(this.x,this.y-70,context);
                    if(this.image.imageCurrentCol == this.image.imageTileColCount-2){
                        // Retour au sprite de base, le idle state
                        this.setImageIdle();
                    }
                }
                else if(this.nomAttaque === "Special1"){
                    // Lorsque le joueur choisi l'attaque Special1 , il lance un projectile qui a sa propre animation
                    if(this.bulletX==400){
                        this.bullet = new TiledImage("images/projectile.png",10, 1, 400, true, 1.4,null);
                        this.bullet .changeRow(0);
                        this.bullet.changeMinMaxInterval(1,10);
                        this.bullet.setLooped(false);
                    }
                    this.bulletX +=7;
                    this.image.tick(this.x,this.y,context);
                    this.bullet.tick(this.bulletX,this.y-100,context);
                    if(this.image.imageCurrentCol == this.image.imageTileColCount-1){
                        this.bulletX=400;
                        this.setImageIdle();
                    }
                }
                else if(this.nomAttaque =="Special2"){
                    if(this.x <700){
                        this.x += 7;
                    }
                    this.image.tick(this.x,this.y,context);
                    if(this.image.imageCurrentCol == this.image.imageTileColCount-2){
                        // Retour au sprite de base, le idle state
                        this.setImageIdle();
                    }

                }
            }
    }
    // Fonction générqique qui permet de changer d'image d'attaque rapidement
    setImage(path, columnCount, rowCount, refreshDelay, loopColumns, scale, minInterval,maxInterval,nomAttack){
        this.nomAttaque = nomAttack;
        this.attacking = true;
        this.image = new TiledImage(path,columnCount, rowCount, refreshDelay, loopColumns, scale,null);
        this.image.changeRow(0);				
        this.image.changeMinMaxInterval(minInterval, maxInterval);
        this.image.setLooped(false);
    }
    // Fonction qui retourne le joueur a son animation de 
    setImageIdle(){
        this.x = 400;
        this.y = 340;
        this.image = new TiledImage("images/idle-player.png",4, 1, 400, true, 1.4,null);
        this.image.changeRow(0);				
        this.image.changeMinMaxInterval(1, 4);
        this.attacking = false;
        this.nomAttaque="";
    }
    
}