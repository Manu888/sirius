class Boss{
    
        constructor(path,columnCount,rowCount,refreshDelay,loopColumns,scale,minInterval,maxInterval){
    
            this.image = new TiledImage(path, columnCount, rowCount, refreshDelay, loopColumns, scale, null);
            this.image.changeRow(0);
            this.image.changeMinMaxInterval(minInterval,maxInterval);
            this.image.setFlipped(true);
            this.attacking = false;
            this.x = 800;
            this.y = 320;
            this.explosion = null;	
            }
    
            tick(context){
                // Le boss a la particularité de générer une explosion qd il touche le groupe d'ennemi
                if(this.explosion){
                    // Si il y a un explosion on l'anime
                    this.explosion.tick(300,this.y,context);
                    if(this.explosion.imageCurrentCol == this.explosion.imageTileColCount-1){
                        this.explosion = null;
                    }
                }
                if (!this.attacking){
                    this.image.tick(this.x,this.y,context); 
                }
                else{
                    // Si le boss est en train d'attaquer, on doit le déplacer vers les ennemis
                    this.x -= 5;
                    this.image.tick(this.x,this.y,context);
                    if(this.x <400){
                        if(this.explosion == null){
                            this.explosion = new TiledImage("images/explosion.png", 7, 1, 200, true, 1.4, null);
                            this.explosion.changeRow(0);				
                            this.explosion.changeMinMaxInterval(1, 7);
                            this.explosion.setLooped(false);
                            this.explosion.setFlipped(true);
                        }
                    }
                    if(this.image.imageCurrentCol+1 == this.image.imageTileColCount){
                        this.setImageIdle();
                    }
                }
                
        }
        setImage(path, columnCount, rowCount, refreshDelay, loopColumns, scale, minInterval,maxInterval){
            this.attacking = true;
            this.image = new TiledImage(path,columnCount, rowCount, refreshDelay, loopColumns, scale,null);
            this.image.changeRow(0);				
            this.image.changeMinMaxInterval(minInterval, maxInterval);
            this.image.setLooped(false);
            this.image.setFlipped(true);
        }
        setImageIdle(){
            this.attacking = false;
            this.x = 800;
            this.y = 320;
            this.image = new TiledImage("images/boss-idle.png",4, 1, 400, true, 1.4,null);
            this.image.changeRow(0);				
            this.image.changeMinMaxInterval(1, 4);
            this.image.setFlipped(true);
            this.attacking = false;
        }
        
    }