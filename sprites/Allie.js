class Allie{
    
        constructor(path,columnCount,rowCount,refreshDelay,loopColumns,scale,minInterval,maxInterval,num, x ,y){
    
            this.image = new TiledImage(path, columnCount, rowCount, refreshDelay, loopColumns, scale, null);
            this.image.changeRow(0);				
            this.image.changeMinMaxInterval(minInterval, maxInterval);
            this.attacking = false;
            this.no = num
            this.x = x;
            this.y = y;
            this.entree = false; 	
            }
    
            tick(context){
                if(this.no == 4 && this.y == 350 && this.attacking){
                    // Sur certaines écran il y a un petit décalage d'une vingtaine de pixel parfois..
                    this.y-=0;
                }
                if(!this.attacking){
                    this.image.tick(this.x,this.y,context); 
                }
                else{
                    if(this.x <700){
                        this.x += 10;
                    }
                    this.image.tick(this.x,this.y,context);
                    if(this.image.imageCurrentCol == this.image.imageTileColCount-2){
                            // Retour au sprite de base, le idle state
                            this.setImageIdle();
                    }
                }
                    
            }
            
        
    
        setImage(path, columnCount, rowCount, refreshDelay, loopColumns, scale, minInterval,maxInterval){
            this.attacking = true;
            this.image = new TiledImage(path,columnCount, rowCount, refreshDelay, loopColumns, scale,null);
            this.image.changeRow(0);				
            this.image.changeMinMaxInterval(minInterval, maxInterval);
            this.image.setLooped(false);
            
        }
        // La fonction de retour a l'état d'attente discrimine selon le numéro du joueur
        // car chaque allié a un sprite différent
        setImageIdle(){
            if (this.no == 2){
                this.x = 300;
                this.y = 340;
                this.image = new TiledImage("images/idle-player2.png",6, 1, 400, true, 1.4,null);
                this.image.changeRow(0);				
                this.image.changeMinMaxInterval(1, 6);
                this.image.setLooped(false);
                this.attacking = false;
            }
            else if (this.no == 3){
                this.x = 225;
                this.y = 340;
                this.image = new TiledImage("images/idle-player3.png",4, 1, 400, true, 1.4,null);
                this.image.changeRow(0);				
                this.image.changeMinMaxInterval(1, 4);
                this.image.setLooped(false);
                this.attacking = false;
            }
            else if(this.no == 4){
                this.x = 125;
                this.y = 350;
                this.image = new TiledImage("images/idle-player4.png",4, 1, 400, true, 1.6,null);
                this.image.changeRow(0);				
                this.image.changeMinMaxInterval(1, 4);
                this.image.setLooped(false);
                this.attacking = false;
            }
        }
        
    }