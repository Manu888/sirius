class Missile {

    constructor(){
        // Chaque missile est un sprite de 5 images
        this.image = new TiledImage("images/missile.png", 5, 1, 400, true, 1.4, null);
        this.image.changeRow(0);				
        this.image.changeMinMaxInterval(1, 5);
        // sa position débute en bordure mais une hauteur et une vitesse aléatoire
        this.x = 0;
        this.y = Math.random()*1000;
        this.speed = Math.random()*20;
    }

    // À chaque tick, le missile se déplace et son image est dessiner
    // Il retourne a JavascriptLobby, si il est encore dans le cadre de l'image
    tick(context){
        let alive = this.x <1920;
        if(alive){
            this.x+=this.speed;
            this.image.tick(this.x,this.y,context);
        }
        return alive;
    }
}