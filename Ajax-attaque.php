<?php
	require_once("action/AjaxAttaqueAction.php");

	$action = new AjaxAttaqueAction();
    $action->execute();
    
    echo json_encode($action->result);
