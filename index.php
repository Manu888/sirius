<?php
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();

	$wrongLogin = $action->wrongLogin;

?>
	<!DOCTYPE html>
	<html lang="fr">
    <head>
        <link href="css/global.css" rel="stylesheet" />
		<title>Sirius</title>
		<meta charset="utf-8" />
		<script src="js/jquery.min.js"></script>
		<script src="js/javascriptIndex.js"></script>
    </head>
    <body>
	<header>
		<h1>Sirius</h1>
		<div class="clear"></div>
	</header>
				
	
    <h2 id="Connexion-Index">Connexion</h2>
    <div class="clear"></div>

	<audio id="audioPlayer">
		<source src="audio\opening.mp3">
	</audio>
	<div class="login-form-frame">
		<form action="index.php" method="post" onsubmit= "return storingUsername()">
			<?php 
			// Si on a un erreur de login, on le fait apparait dans une section erreur
				if ($wrongLogin) {
					?>
					<div class="error-div"><strong>Erreur : </strong><?= $wrongLogin ?></div>
					<?php
				}
			?>

			<div class="form-label">
				<label for="username">Nom d'usager : </label>
			</div>
			<div class="form-input">
				<input type="text" name="username" id="username" />
			</div>
			<div class="form-separator"></div>

			<div class="form-label">
				<label for="password">Mot de passe : </label>
			</div>
			<div class="form-input">
				<input type="password" name="password" id="password" />
			</div>
			<div class="form-separator"></div>

			<div class="form-label">
				&nbsp;
            </div>
            
			<div class="form-input">
				<button type="submit">Connexion</button>
			</div>
			<div class="form-separator"></div>
		</form>
    </div>
    <canvas id="canvas" width="1920" height="1080">
    </canevas>

<?php
	require_once("partial/footer.php");
