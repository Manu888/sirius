<?php
	require_once("action/LoseAction.php");

	$action = new LoseAction();
	$action->execute();


?>

<!DOCTYPE html>
	<html lang="fr">
    <head>
        <link href="css/global.css" rel="stylesheet" />
		<title>Sirius</title>
		<meta charset="utf-8" />
		<script src="js/jquery.min.js"></script>
		<script src="js/javascriptLost.js"></script>
    </head>
    <body id="lost">
	<header>
		<h1>Sirius</h1>
		<div class="clear"></div>
	</header>
    <h2 id="losttext">YOU LOSE  !</h2>
	<audio id="audioLost">
		<source src="audio\lose.mp3">
	</audio>
    <?php
	require_once("partial/footer.php");