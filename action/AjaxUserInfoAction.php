<?php
    require_once("action/CommonAction.php");

	class AjaxUserInfoAction extends CommonAction {
		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
            if(isset($_SESSION["key"])){
				$data = [];
				$data["key"] = $_SESSION["key"];
				
				$this->result = AjaxUserInfoAction::callAPI("user-info", $data);
				}
				else{
					header("location:index.php");
					exit;
			}
		}
        
	}