<?php
	require_once("action/CommonAction.php");

	class IndexAction extends CommonAction {
		public $wrongLogin = false;
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {

			// Si on a un nom et un mot de passe on tente de se connecter à l'API
			if (isset($_POST["username"]) && isset($_POST["password"])) {

				$data = [];
				$data["username"] = $_POST["username"];
				$data["pwd"] = $_POST["password"];
				
				$key = IndexAction::callAPI("signin", $data);
				// Si l'élément de retour est d'une longueur de 40 caracteres, nous avons la clé d'entrée
				if (strlen($key) == 40) {
					// On stocke les valeurs nécessaires et on se dirige au lobby
					$_SESSION["key"] = $key;
					$_SESSION["visibility"] = 1;
					$_SESSION["username"] = $_POST["username"];
					
					header("location:lobby.php");
					exit;
				}
				else {
					// sinon on retour le message d'erreur afin qu'il soit afficher
					$this->wrongLogin = $key;
				}
			}
		}
	}
