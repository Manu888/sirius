<?php
	require_once("action/CommonAction.php");

	class LobbyAction extends CommonAction {
		
		public $result;

		public function __construct() {
			parent::__construct(parent::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			// Si un id de game est envoyer, on tente de se connecter à cette partie
			if (isset($_POST["id_game"])){
		
				$data = [];
				$data["key"] = $_SESSION["key"];
				$data["id"] = $_POST["id_game"];
				
				$this->result = LobbyAction::callAPI("enter", $data);

				if (gettype($this->result) !== "object") {
					if ($this->result == "GAME_ENTERED") 
					{		// Si on est bien dans la partie, on se dirige vers game.php
							$_SESSION["id_game"] = $_POST["id_game"];
							header("location:game.php");
							exit;
					}
				}
					
				
		
			
        }
	}
}
	
