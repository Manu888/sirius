<?php
    require_once("action/CommonAction.php");

	class AjaxGameInfoAction extends CommonAction {
		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			
			if(isset($_SESSION["key"])){
            $data = [];
            $data["key"] = $_SESSION["key"];
            
			$this->result = AjaxGameInfoAction::callAPI("state", $data);
			}
			else{
				header("location:index.php");
				exit;
			}
		}
        
	}