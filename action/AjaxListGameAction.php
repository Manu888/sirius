<?php
    require_once("action/CommonAction.php");

	class AjaxListGameAction extends CommonAction {
		public $result;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
            if(isset($_SESSION["key"])){
				$data = [];
				$data["key"] = $_SESSION["key"];
				
				$this->result = AjaxListGameAction::callAPI("list", $data);
				}
			
			else{
				header("location:index.php");
				exit;
			}
        }
	}