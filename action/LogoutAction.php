<?php
	require_once("action/CommonAction.php");

	class LogoutAction extends CommonAction {
		public $wrongLogin = false;
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC);
		}

        protected function executeAction() {
            // On prend la clé stocker et on déconnecte le joueur qui en a fait la demande
            $data = [];
            $data["key"] = $_SESSION["key"];
            
            $key = LogoutAction::callAPI("signout", $data);
            header("location:index.php");
            exit;
        }
        }