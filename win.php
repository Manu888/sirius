<?php
	require_once("action/WinAction.php");

	$action = new WinAction();
	$action->execute();


?>

<!DOCTYPE html>
	<html lang="fr">
    <head>
        <link href="css/global.css" rel="stylesheet" />
		<title>Sirius</title>
		<meta charset="utf-8" />
		<script src="js/jquery.min.js"></script>
		<script src="js/javascriptWin.js"></script>
    </head>
    <body id="win">
	<header>
		<h1>Sirius</h1>
		<div class="clear"></div>
	</header>
	<audio id="audioWin">
		<source src="audio\win.mp3">
	</audio>
    <h2 id="wintext">YOU WIN  !</h2>

    <?php
	require_once("partial/footer.php");