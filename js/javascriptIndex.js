let context = null;
let canvas = null;
let spritelist = []
let img = null;
let profondeurMax = 32;
var stars = new Array(50);
let width = null;
let height = null;

window.onload = function(){
  // Si on a un nom de stocker dans le local storage, on le met dans le champ username
  if(localStorage["username"] != null){
    $("#username").val(localStorage["username"])
  }
  // On démarre le champ d'introduction au login
    let player = document.querySelector('#audioPlayer');
    player.play();

    // On récupere le context et on initialise l'image de fond
    canvas = document.getElementById("canvas"); 
    context = canvas.getContext("2d");
    width = window.innerWidth;
    height = window.innerHeight;
	  img = new Image();
	  img.src = "images/Accueil_background.jpg";

    

    pluieEtoile();
    generalTick();
}
// Lorsqu'on se connecte, on recupere le champ username et on le stock en localstorage
 function storingUsername (){
  localStorage["username"] = document.getElementById("username").value;
}
 
   
    /* Retourne un numéro de min a max*/ 
   function randomRange(minVal,maxVal) {
     return Math.floor(Math.random() * (maxVal - minVal - 1)) + minVal;
   }

   // On initialise des position aléaloire pour  les 50 étoiles
   function pluieEtoile() {
     // On initialise les 3 valeurs (x,y,z) des étoiles
     for( var i = 0; i < stars.length; i++ ) {
       stars[i] = {
         
         x: randomRange(-25,25),
         y: randomRange(-25,25),
         z: randomRange(1,profondeurMax)
        }
     }
   }

   function generalTick() {
     let demiLargeur  = width / 2;
     let demiHauteur = height / 2;
    // On dessine le fond d'écran
     context.drawImage(img,0,0)

     for( var i = 0; i < stars.length; i++ ) {
        // Chaque étoile se rapproche
       stars[i].z -= 0.2;

       // Si les étoile touche la profondeur zero, on repositionne l'étoile au loin
       if( stars[i].z <= 0 ) {
         stars[i].x = randomRange(-25,25);
         stars[i].y = randomRange(-25,25);
         stars[i].z = profondeurMax;
       }
       // On calcul une vitesse selon sa profondeur
       let k  = 128.0 / stars[i].z;
       // On calcule la nouvelle poisition de l'étoile selon sa vitesse
       let px = stars[i].x * k + demiLargeur;
       let py = stars[i].y * k + demiHauteur;

       // Si l'étoile est toujours dans le cadran de l'écran, on la redessine
       if( px >= 0 && px <= width && py >= 0 && py <= height ) {
         let taille = (1 - stars[i].z / 32.0) * 5;
         let couleur = parseInt((1 - stars[i].z / 32.0) * 255);
         context.fillStyle = "rgb(" + couleur + "," + couleur + "," + couleur + ")";
         context.fillRect(px,py,taille,taille);
       }

       
     }
      window.requestAnimationFrame(generalTick);
    }
