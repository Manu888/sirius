let canvas = null;
let context = null;
let img = null;
let missiles = [];

$(function(){

    // On recupere le context du canevas et on charge l'image de fond
    canvas = document.getElementById("canvas"); 
    context = canvas.getContext("2d");
	img = new Image();
    img.src = "images/Lobby_back.jpg";

    
    // On démarre la musique de Lobby
    let player = document.querySelector('#audioLobby');
    player.play();
    
    generalTick();


    function generalTick(){
        // On dessine l'image
        if(img.complete){ context.drawImage(img,0,0);}

        // Il y a 90% de chance a chaque tick de faire apparaitre un missile
        let test = Math.random();
        if(test>=0.9)
            // On ajoute le missile a la liste
			missiles.push(new Missile());
		for (let i = 0; i < missiles.length; i++){
            // Pour chaque missile, si il est toujours en vie, on le dessine
			let alive = missiles[i].tick(context);
			if(!alive){
                // Sinon on l'enlève de la liste de missiles
				missiles.splice(i--,1);
			}
		}

        window.requestAnimationFrame(generalTick);
        }


    }
)
