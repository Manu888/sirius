let att = false;
let bossAttaque = false;
let allieAttaque = [false,false,false]
let nomAttaque;
let noAttaque;
let joueur1, joueur2, joueur3 ,joueur4, boss;
let nombreJoueur = 1;

function attaque(skillname){
    $.ajax({
        url:"ajax-attaque.php",
        type:"POST",
        data:{"skill-name": skillname}
    })
    .done(function(data){
        let resultat = JSON.parse(data);
        // On attend 2 secondes apres la réception du JSON afin de rendre actif le bouton d'attaque
        setTimeout(function(){
            // Le joueur peut maintenant attaquer de nouveau
                att = false;
        },2000)
        if (typeof resultat !== "object"){
            if(resultat === "OK"){
                // On stocke le nom de l'attaque afin de faire l'animation correspondantes
                nomAttaque = skillname; 
            }
            else if(resultat =="NOT_ENOUGH_MP"){
                // Si on a pas assez de MP, on l'affiche au joueur
                let erreur = document.getElementsByClassName("error-div");
                erreur[0].innerHTML = "Vous n'avez plus assez de MP !";
                $(".error-div").fadeIn("slow").fadeOut("slow");
            }
    }

    })
    .fail(function (data) {
        alert("Aïe, erreur avec le serveur");
    })
}

$(function(){
    // On récupere le context du canevas et on charge l'image de fond
    canvas = document.getElementById("jeu"); 
    context = canvas.getContext("2d");
	imageBack = new Image();
    imageBack.src = "images/back.gif";
    
    // On démarre la musique de combat
    let player = document.querySelector('#audioGame');
    player.play();
    
    // On initialise toutes les joueurs dans leur animation de combat en attente
    joueur1 = new Joueur("images/idle-player.png",4, 1, 400, true, 1.4, 1 , 4 );
    
    boss = new Boss("images/boss-idle.png",4, 1, 400, true, 1.4,1,4);
    
    joueur2 = new Allie("images/idle-player2.png", 6, 1, 400, true, 1.4, 1 , 6 , 2, 300 , 340);
    
    joueur3 = new Allie("images/idle-player3.png", 4, 1, 400, true, 1.4, 1, 4, 3, 225, 340);

    joueur4 = new Allie("images/idle-player4.png", 4, 1, 400, true, 1.6, 1, 4, 4, 125, 350);

    generalTick();

    

function generalTick(){
    // On dessine le fond si l'image est chargée
    if(imageBack.complete){ 
        context.drawImage(imageBack,0,0);
    }
    // On montre le portrait du joueur 1 et du boss
    $(".portrait:first").show();
    $(".portrait:last").show();
    joueur1.tick(context);
    boss.tick(context);
    // Pour chacune des attaques l'animation correspondantes est selectionné puis on réinitialise le nom de l'attaque
    if(nomAttaque === "Normal"){
        joueur1.setImage("images/player1-attack1.png",15,1,200,true,1.4,1,14,nomAttaque);
        nomAttaque = "";
    }
    else if(nomAttaque === "Special1"){
        joueur1.setImage("images/player1-attack2.png",7,1,200,true,1.4,1,7,nomAttaque);
        nomAttaque = "";
    }
    else if(nomAttaque == "Special2"){
        joueur1.setImage("images/player1-attack3.png",13,1,200,true,1.4,1,13,nomAttaque);
        nomAttaque="";
    }
    // Si le boss attaque, on démarre son animation d'attaque
    if(bossAttaque){
        boss.setImage("images/boss-attack.png",11,1,200,true,1.4,1,11);
        bossAttaque=false;
    }

    // Pour chacune des joueurs alliés, on vérifie si il vien d'entrée afin de faire l'animation de son portrait
    // sinon on affiche son portrait simplement
    if(nombreJoueur>=2){
        if(!joueur2.entree){
            setTimeout(function(){joueur2.entree =true;}, 5000);
            $(".portrait:eq(1)").fadeIn("slow");
        }
        else{
            $(".portrait:eq(1)").show();
        }
        joueur2.tick(context);
    }
    // Si le premier alliee attaque, on selectionne son sprite d'attaque puis on réinitilise son attaque
    if(allieAttaque[0]){
        joueur2.setImage("images/player2-attack.png",8,1,200,true,1.4,1,8);
        allieAttaque[0]=false;
    }
    // Le principe est le même pour toutes les autres joueurs.. portrait d'entrée qui s'anime puis on change pour l'animation d'attaque...
    if(nombreJoueur>=3){
        if(!joueur3.entree){
            setTimeout(function(){joueur3.entree =true;}, 5000);
            $(".portrait:eq(2)").fadeIn("slow");
        }
        else{
            $(".portrait:eq(2)").show();
        }
        joueur3.tick(context);
    }
    if(allieAttaque[1]){
        joueur3.setImage("images/player3-attack.png",7,1,200,true,1.4,1,7);
        allieAttaque[1]=false;
    }
    if(nombreJoueur==4){
        if(!joueur4.entree){
            setTimeout(function(){joueur4.entree =true;}, 5000);
            $(".portrait:eq(3)").fadeIn("slow");
        }
        else{
            $(".portrait:eq(3)").show();
        }
        joueur4.tick(context);
    }
    if(allieAttaque[2]){
        joueur4.setImage("images/player4-attack.png",10,1,200,true,1.6,1,10);
        allieAttaque[2]=false;
    }
    window.requestAnimationFrame(generalTick);
    }


}
)