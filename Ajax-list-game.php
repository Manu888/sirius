<?php
	require_once("action/AjaxListGameAction.php");
    // On execute la demande de liste de partie
	$action = new AjaxListGameAction();
    $action->execute();
    // Ce qui nous est retourné est encoder en JSON
    echo json_encode($action->result);
