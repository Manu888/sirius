<?php
	require_once("action/AjaxUserInfoAction.php");
    // On execute la demande d'information
	$action = new AjaxUserInfoAction();
    $action->execute();
    // Ce qui nous est retourner est encoder en JSON
    echo json_encode($action->result);
